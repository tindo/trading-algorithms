//+------------------------------------------------------------------+
//|                                             Floor Trader System  |
//|                          Copyright � 2011, Wilson Mazaiwana.     |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2011, BlackMarket Holdings."
#property link      "http://www.bmholdings.com"

//---- input parameters
extern int       take_profit=30;
extern int       stop_loss=30;
extern bool      MoneyManagement =TRUE;

extern double    Trade_Offset = 0.5;
extern int       Slow = 30;
extern int       Fast = 9;
extern int       Min  = 6;
extern int       OpenHour =0;
extern int       CloseHour = 18;
extern int       SwitchPeriod = 4;
extern int       Conditioner = 20;
extern int       tradestotal =4;

//---global variables
int magic_number = 543212345;
string _time_opened_1=""; //we use this string as the name of our GV to keep track of the time the last trade opened
//string _time_opened_2=""; //we use this string as the name of our GV to keep track of the time the trade prior to the last trade opened
double _my_point=0.0001; //we use this variable to consider the Pip value in decimal points


//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----
   
   //I define and initiate a GV that tracks the time we have opened a trade.
   //I like GVs because even if you close MetaTrader for up to 4 weeks their value
   //remain intact. 
   //the first step is to choose a unique name for the GV. I use a combination of 
   //a fixed string, the chart symbol and the magic number
   _time_opened_1=StringConcatenate("DMA_1_",Symbol(),magic_number);
   //_time_opened_2=StringConcatenate("DMA_2_",Symbol(),magic_number);

   //if the variable exists then we won't change it but if it does not exist then we assign
   //0 to it.
   if (!GlobalVariableCheck(_time_opened_1)) GlobalVariableSet(_time_opened_1,0);
   //the following line helps in back-testing the system
   if (GlobalVariableGet(_time_opened_1)>TimeCurrent()) GlobalVariableSet(_time_opened_1,0);
   //if (!GlobalVariableCheck(_time_opened_2)) GlobalVariableSet(_time_opened_2,0);

//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+
int start()
  {
//----
   if (OrdersTotal()>0)
      {

      CloseTrades(1);
      }

      TradeSignal();

        
//----
   return(0);
  }
  
  void DebugSpread(){
      Print("The current spread is "+MarketInfo(Symbol(),MODE_SPREAD)+"  Exchange Rate: "+MarketInfo("GBPUSD",MODE_BID));
      return;
  }
//+------------------------------------------------------------------+
//| Define which times to trade                                      |
//+------------------------------------------------------------------+ 
  int TradeTime() {
   bool do_it = true;
   if (OpenHour > CloseHour)
      if (Hour() < CloseHour || Hour() >= OpenHour) do_it = TRUE;
   if (OpenHour < CloseHour)
      if (Hour() >= OpenHour && Hour() < CloseHour) do_it = TRUE;
   if (OpenHour == CloseHour)
      if (Hour() == OpenHour) do_it = TRUE;
   //if (DayOfWeek() == 5 && Hour() > 6) do_it = true;
   //if (DayOfWeek() == 1 && Hour() < 3) do_it = true;

   return (do_it);
}
//+------------------------------------------------------------------+
//Open multiple trades if you want, but NO MULTIPLE SAME CURRENCY TRADES
//+------------------------------------------------------------------+
int OneTradePerCurrency(string symbol){
    bool go_ahead = true;
    int i=OrdersTotal()-1;
  if(i>=0){
   OrderSelect(i, SELECT_BY_POS,MODE_TRADES);
   if(OrderSymbol()==symbol && (OrderType()==OP_BUYSTOP|| OrderType()==OP_SELLSTOP||OrderType()==OP_BUY||OrderType()==OP_SELL)&&OrderOpenTime()>=Time[0])
      {   
          go_ahead = false; // FALSE to abandon trade placement, TRUE to nullify the effect of this function
      }
    }
    return (go_ahead);
}

//+------------------------------------------------------------------+
// Screen last trade to see if it was a loser, then decide what type of trade is going to be next
//+------------------------------------------------------------------+

int LastClosedTrade(int p_signal){
    bool go_ahead = true;

    
    if(p_signal==0){ int p_type=OP_SELLSTOP;}
    if(p_signal==1) {p_type=OP_BUYSTOP;}
    int i=OrdersHistoryTotal()-1;
//for(int i=OrdersHistoryTotal()-1;i>=0;i--)
 //{
  if(i>=0){
   OrderSelect(i, SELECT_BY_POS,MODE_HISTORY);
   if(OrderSymbol()==Symbol()  && OrderType()==p_type && TimeMinute(TimeCurrent())-TimeMinute(OrderOpenTime())<PERIOD_M1)
    {   
        go_ahead = false; // FALSE to abandon trade placement, TRUE to nullify the effect of this function
    }
  //}
    }
    return (go_ahead);
}


//+------------------------------------------------------------------+
//Lot Management
//+------------------------------------------------------------------+
double GetLots() {
   
   double exchangerate;

	if (AccountCurrency()=="GBP"){exchangerate = MarketInfo("GBPUSD",MODE_BID);}
	else {exchangerate = 1;}
    if (MoneyManagement == TRUE)
	   {
	   if ((AccountBalance()>=10))
		  {
		  if (AccountEquity()<AccountBalance()){
		    double LotsPerTrade = NormalizeDouble((AccountEquity()*exchangerate/10000),2); //10000 ==> approx 2% per trade
		  }else{
		    LotsPerTrade = NormalizeDouble((AccountBalance()*exchangerate/10000),2);
		    } // Print("Exchange Rate is "+exchangerate+" and lots are "+LotsPerTrade); Debug exchange rate
		  } 
	   else
		  {
		  LotsPerTrade = 0.01;
		  }
	   }
	   else
	   {
		  LotsPerTrade = (0.01);
		  
	   }
 
 return (LotsPerTrade);
 }
 

//+------------------------------------------------------------------+
//TRade Signal
//+------------------------------------------------------------------+
void TradeSignal()
      {
       _my_point=Point;
       if (_my_point==0.00001) _my_point=0.0001;
       if (_my_point==0.001) _my_point=0.01;
      double Threshold = Min*_my_point;
      double Enter = 0*_my_point;
      double Offset = Trade_Offset * _my_point;
      
      //double Market_Condition = iCustom(Symbol() , 0 , "H-Spool" , Conditioner , 50 , 2 ,1);
      double BigMA  = iMA(Symbol(),PERIOD_H1,9,0,MODE_EMA,PRICE_CLOSE,1); 
      double PreCross  = iMACD(Symbol(),0,9,Slow,1,PRICE_CLOSE,MODE_EMA,2);
      double PostCross = iMACD(Symbol(),0,9,Slow,1,PRICE_CLOSE,MODE_EMA,1);
      double FasterMA   = iMA(Symbol(),0,9,0,MODE_EMA,PRICE_CLOSE,1);
      double SlowerMA   = iMA(Symbol(),0,Slow,0,MODE_EMA,PRICE_CLOSE,1);
      //double UpperBand  = iBands(Symbol(),0,7,1,0,PRICE_CLOSE,MODE_UPPER,1);
      //double LowerBand  = iBands(Symbol(),0,7,1,0,PRICE_CLOSE,MODE_LOWER,1);
      double Momo = iCustom(Symbol(),0,"Wilson\'s Gradient",27,4,0,1);

      double Mini = iMomentum(Symbol(),0,14,PRICE_CLOSE,0);
     if(PreCross>0 && PostCross<0){CloseTrades(0);}
     if(PreCross<0 && PostCross>0){CloseTrades(0);}
     
      if (OrdersTotal()<=tradestotal) //FTS CPU ....No trade overload, but modifying still goes on 247
      {
         if((Momo>0.7 && Mini>100.08)&&(TradeTime()==1)&& High[0]>(High[1]+Enter)){int trade=1;}
         else if((Momo<-0.7 && Mini<99.92)&&(TradeTime()==1)&&Low[0]<(Low[1]-Enter)){trade =2;}
         else{trade =0;}
      }
  if((PostCross>Threshold || PostCross<-Threshold)/**/){   
//================
//3 BUY CONDITIONS 
//================

     if((/*PreCross<0 &&*/ PostCross>0)&& (/*Low[3]>Low[2]&&*/Low[2]>Low[1])/* && Low[1]<=LowerBand*/ && Close[0]>SlowerMA){ //CROSS UPWARDS FOR LONGS 
        if(Low[0]<Low[1]){double Stop=Low[0]-Enter;}
        else{Stop=Low[1]-Enter;}  
          if(Low[1]>=FasterMA && Low[1]<FasterMA+Offset) //LEVEL 3 BUY AS ON DOCUMENTATION
             {
             CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==1){
                  /*if(LastClosedTrade(0)==1)*/OpenTrade(1,GetLots(),Stop,"Level 3 "+Symbol());
                  // if(LastClosedTrade(0)==0){Print("Sorry, You cant BUY");}
                  }
             }
       
          if(Low[1]<FasterMA && Low[1]>SlowerMA)//LEVEL 2 BUY AS ON DOCUMENTATION
             {
             CloseTrades(2);//CANCEL CURRENT PLACED LEVEL 3 ORDER FIRST
                  if(OneTradePerCurrency(Symbol())==1 && trade==1){
                  /*if(LastClosedTrade(1)==1)*/OpenTrade(1,GetLots(),Stop,"Level 2 "+Symbol());
                  // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                  }
             }
             
          if(Low[1]<=SlowerMA)
             {
             if (Low[0]<Low[1]){CloseTrades(3);}
             if (Low[0]>Low[1]){
               CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==1){
                  /*if(LastClosedTrade(1)==1)*/OpenTrade(1,GetLots(),Stop,"Level 1 "+Symbol());
                  // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                  }
              }
             }
     }
 
//===============================================
//3 BUY MORE CONDITIONS (INCASE OF INSIDE CANDLE)
//===============================================
     if((/*PreCross<0 &&*/ PostCross>0)&& (Low[3]>Low[2]&&Low[2]<Low[1])&&High[2]>High[1]&& Close[1]>SlowerMA){ //CROSS UPWARDS FOR LONGS 
        if(Low[0]<Low[2]){ Stop=Low[0]-Enter;}
        else{Stop=Low[2]-Enter;}  
          if(Low[2]>=FasterMA && Low[2]<FasterMA+Offset) //LEVEL 3 BUY AS ON DOCUMENTATION
             {
             CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==1){
                  /*if(LastClosedTrade(0)==1)*/OpenTrade(1,GetLots(),Stop,"Level 3A "+Symbol());
                  // if(LastClosedTrade(0)==0){Print("Sorry, You cant BUY");}
                  }
             }
       
          if(Low[2]<FasterMA && Low[2]>SlowerMA)//LEVEL 2 BUY AS ON DOCUMENTATION
             {
             CloseTrades(2);//CANCEL CURRENT PLACED LEVEL 3 ORDER FIRST
                  if(OneTradePerCurrency(Symbol())==1 && trade==1){
                  /*if(LastClosedTrade(1)==1)*/OpenTrade(1,GetLots(),Stop,"Level 2A "+Symbol());
                  // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                  }
             }
             
          if(Low[2]<=SlowerMA)
             {
             if (Low[1]<Low[2]){CloseTrades(3);}
             if (Low[1]>Low[2]){
               CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==1){
                  /*if(LastClosedTrade(1)==1)*/OpenTrade(1,GetLots(),Stop,"Level 1A "+Symbol());
                  // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                  }
              }
             }
     }

//=================
//3 SELL CONDITIONS
//=================

     if((/*PreCross>0 &&*/ PostCross<0) && (/*High[3]<High[2]&&*/High[2]<High[1])/*&& High[1]>UpperBand*/ && Close[0]<SlowerMA){ //CROSS DOWNWARDS FOR SHORTS 
       if(High[0]>High[1]){Stop = High[0]+Enter;}
       else {Stop = High[1]+Enter;} 
          if(High[1]<=FasterMA && High[1]>FasterMA-Offset) // 0.0001 = 1 pip, for 5-decimal digit currencies
             {
             CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==2){
                   /*if(LastClosedTrade(0)==1)*/OpenTrade(0,GetLots(),Stop,"Level 3 "+Symbol());
                  // if(LastClosedTrade(0)==0){Print("Sorry, You cant SELL");}
                  }
             }
       
          if(High[1]>FasterMA && High[1]<SlowerMA)
             {
              CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==2){
                  /*if(LastClosedTrade(1)==1)*/OpenTrade(0,GetLots(),Stop,"Level 2 "+Symbol());
                  // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                  }
             }
             
          if(High[1]>=SlowerMA)
             {
               if (High[0]>=High[1]){CloseTrades(3);}
               if (High[0]<High[1]){
                CloseTrades(2);
                     if(OneTradePerCurrency(Symbol())==1 && trade==2){
                     /*if(LastClosedTrade(1)==1)*/OpenTrade(0,GetLots(),Stop,"Level 1 "+Symbol());
                     // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                     }
                }
             }
     }  
    
//================================================
//3 MORE SELL CONDITIONS (INCASE OF INSIDE CANDLE)
//================================================
     if((/*PreCross>0 &&*/ PostCross<0) && (High[3]<High[2]&&High[2]>High[1])&&Low[2]<Low[1]/*&& High[1]>UpperBand*/ && Close[1]<SlowerMA){ //CROSS DOWNWARDS FOR SHORTS 
       if(High[0]>High[2]){Stop = High[0]+Enter;}
       else {Stop = High[2]+Enter;} 
          if(High[2]<=FasterMA && High[2]>FasterMA-Offset) // 0.0001 = 1 pip, for 5-decimal digit currencies
             {
             CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==2){
                   /*if(LastClosedTrade(0)==1)*/OpenTrade(0,GetLots(),Stop,"Level 3A "+Symbol());
                  // if(LastClosedTrade(0)==0){Print("Sorry, You cant SELL");}
                  }
             }
       
          if(High[2]>FasterMA && High[2]<SlowerMA)
             {
              CloseTrades(2);
                  if(OneTradePerCurrency(Symbol())==1 && trade==2){
                  /*if(LastClosedTrade(1)==1)*/OpenTrade(0,GetLots(),Stop,"Level 2A "+Symbol());
                  // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                  }
             }
             
          if(High[2]>=SlowerMA)
             {
              // if (High[1]>=High[2]){CloseTrades(3);}
               if (High[1]<High[2]){
                CloseTrades(2);
                     if(OneTradePerCurrency(Symbol())==1 && trade==2){
                     /*if(LastClosedTrade(1)==1)*/OpenTrade(0,GetLots(),Stop,"Level 1A "+Symbol());
                     // if(LastClosedTrade(1)==0){Print("Sorry, You cant BUY");}
                     }
                }
             }
     }         
     
}
       
      return;
      }


//+------------------------------------------------------------------+
//Buy/Sell some Shiznit!!
//+------------------------------------------------------------------+ 
   
 void OpenTrade(int buysignal, double LotsPerTrade,double Stop, string YourComment)
      {
         _my_point=Point;
         if (_my_point==0.00001) _my_point=0.0001;
         if (_my_point==0.001) _my_point=0.01; 
          if (buysignal == 1) // If signal is one, buy the ting tingz
          {
          double SL=Ask-(stop_loss*_my_point);
          double TP=Ask+(take_profit*_my_point);
          double Enter = 3 * _my_point;
          //int ticket=OrderSend(Symbol(),OP_BUY,LotsPerTrade,Ask,3,SL,TP,"Same ol\' Sh**",magic_number,0,White);
          int ticket=OrderSend(Symbol(),OP_BUY,LotsPerTrade,Ask,3,Stop,0,YourComment,magic_number,0,White);
         
                  //registering the time the new order is opened
                  if (ticket>=0) 
                     {
                        OrderSelect(ticket,SELECT_BY_TICKET);
                        datetime open_time=OrderOpenTime();
                        GlobalVariableSet(_time_opened_1,open_time);
                     }
          }
          
          if (buysignal == 0) //if Signal is zero, sell the ting tingz
          {
               SL=Bid+(stop_loss*_my_point);
               TP=Bid-(take_profit*_my_point);
               //ticket=OrderSend(Symbol(),OP_SELL,LotsPerTrade,Bid,3,SL,TP,"Jus a different day",magic_number,0,Magenta);
               ticket=OrderSend(Symbol(),OP_SELL,LotsPerTrade,Bid,3,Stop,0,YourComment,magic_number,0,Magenta);
         
         //registering the time the new order is opened
               if (ticket>=0) 
                  {
                     OrderSelect(ticket,SELECT_BY_TICKET);
                     open_time=OrderOpenTime();
                     GlobalVariableSet(_time_opened_1,open_time);
                  }

          }
     }
     


//+------------------------------------------------------------------+
//Close trades conditions
//+------------------------------------------------------------------+
void CloseTrades(int TradeType){
         _my_point=Point;
         if (_my_point==0.00001) _my_point=0.0001;
         if (_my_point==0.001) _my_point=0.01;
         double Enter = 10*_my_point;
   
      /*double PreCross  = iMACD(Symbol(),0,9,Slow,1,PRICE_CLOSE,MODE_EMA,2);
      double PostCross = iMACD(Symbol(),0,9,Slow,1,PRICE_CLOSE,MODE_EMA,1);
      double FasterMA   = iMA(Symbol(),0,9,0,MODE_EMA,PRICE_CLOSE,0);
      double SlowerMA   = iMA(Symbol(),0,Slow,0,MODE_EMA,PRICE_CLOSE,0);  */
      double Momo = iCustom(Symbol(),0,"Wilson\'s Gradient",27,4,0,1);
      double Mini = iMomentum(Symbol(),0,14,PRICE_CLOSE,1);
      
   
  
      for (int i=0;i<OrdersTotal();i++)
      {
         if(!OrderSelect(i,SELECT_BY_POS,MODE_TRADES)) Print("I failed selecting a trade");
         int order_magic=OrderMagicNumber();
         string order_symbol=OrderSymbol();
         int order_ticket=OrderTicket();
         if (TradeType==0){
         OrderDelete(order_ticket); //DELETE PENDING ORDERS
         }
         
         //CLOSE OPEN ORDERS WHEN CONDTITIONS ARE MET
         if ((order_magic==magic_number)&&(order_symbol==Symbol()))
            {
               
               double order_size=OrderLots();
               int order_type=OrderType();
               datetime open_time=OrderOpenTime();
               double open_price = OrderOpenPrice();

               //if user changes the value of GV by mistake the following lines make necessary corrections
               datetime registered_time=GlobalVariableGet(_time_opened_1);
               if (open_time>registered_time)GlobalVariableSet(_time_opened_1,open_time);
               //
               if(TradeType==1){

                     //if(Close[2]>Close[1]&&Close[2]<Open[2]&&Close[1]<Open[1]&&High[0]<=High[1]){
                     //if(Close[0]<=SlowerMA && Close[1]>Close[0]){
                     if(Momo<0 && Mini<100){
                          if (order_type==OP_BUY)
                             OrderClose(order_ticket,order_size,Bid,3,CLR_NONE);
                      
                         }
                   
                     //if(Close[2]<Close[1]&&Close[2]>Open[2]&&Close[1]>Open[1]&&Low[0]>=Low[1]){ 
                    // if(Close[0]>=SlowerMA && Close[1]<Close[0]){
                    if(Momo>0 && Mini>100){
                          if (order_type==OP_SELL)
                             OrderClose(order_ticket,order_size,Ask,3,CLR_NONE); 
                         }    
              
                       
                }
                
                if(TradeType==2){
                     if(order_type==OP_BUY){
                        //if(OrderStopLoss()<(Low[1]-Enter)){
                           OrderModify(OrderTicket(),OrderOpenPrice(),Low[1]-Enter,OrderTakeProfit(),0,Turquoise);
                        //}
                     }
                
                     if(order_type==OP_SELL){
                    // if(OrderStopLoss()>(High[1]-Enter)){
                           OrderModify(OrderTicket(),OrderOpenPrice(),High[1]+Enter,OrderTakeProfit(),0,Turquoise);
                       //  }
                     }
                }
                
                if(TradeType==3){ //FOR CLOSING ORDERS, IF ANY ARE INTRODUCED IN THE FUTURE
                     if(order_type==OP_BUYSTOP){
                        
                           OrderModify(OrderTicket(),High[0]+Enter,OrderOpenPrice(),OrderTakeProfit(),0,Orange);
                        
                     }
                
                     if(order_type==OP_SELLSTOP){
                     
                           OrderModify(OrderTicket(),Low[0]-Enter,OrderOpenPrice(),OrderTakeProfit(),0,Orange);
                         
                     }
                }
            }
         }

}   

